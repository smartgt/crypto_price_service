package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"crypto_price_service/internal/pb"

	"github.com/joho/godotenv"
	"github.com/urfave/cli/v2"
	"google.golang.org/grpc"
)

type instrumentData struct {
	CurrentPrice     float64
	AvgPrice         float64
	PriceHistory     []float64
	MeasurementCount int // Количество измерений
}

type server struct {
	pb.UnimplementedCryptoPriceServiceServer
	instrumentsData map[string]*instrumentData
	mu              sync.RWMutex // мьютекс для синхронизации доступа к данным
}

// Функция для получения текущей цены заданного инструмента с помощью CoinGecko API.
func getPriceFromCoinGecko(instrument string) (float64, error) {
	// Формируем URL запроса к CoinGecko API с использованием переданного инструмента
	apiURL := fmt.Sprintf("https://api.coingecko.com/api/v3/simple/price?ids=%s&vs_currencies=usd", instrument)

	// Отправляем GET-запрос к CoinGecko API
	resp, err := http.Get(apiURL)
	if err != nil {
		return 0.0, err
	}
	defer resp.Body.Close()

	// Обрабатываем ответ и извлекаем цену из JSON-ответа
	var data map[string]map[string]float64
	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return 0.0, err
	}

	// Получаем цену из полученного JSON-ответа
	if price, ok := data[instrument]["usd"]; ok {
		return price, nil
	}
	return 0.0, fmt.Errorf("инструмент не найден: %s", instrument)
}

// Функция для вычисления среднего значения цены по всем инструментам.
// Эта функция будет работать в фоновой горутине.
func (s *server) calculateAverage() {
	ticker := time.NewTicker(5 * time.Second) // обновление каждые 5 секунд
	defer ticker.Stop()

	for range ticker.C {
		var total float64
		var count int
		countData := make(map[string]int) // Карта для хранения количества измерений для каждого инструмента

		// Захватываем мьютекс для безопасного доступа к данным всех инструментов
		s.mu.RLock()
		for instrument, data := range s.instrumentsData {
			total += data.CurrentPrice
			countData[instrument] = len(data.PriceHistory) // Сохраняем количество измерений для инструмента
			count++
		}
		s.mu.RUnlock()

		if count > 0 {
			// Вычисляем среднее значение
			avg := total / float64(count)

			// Захватываем мьютекс для записи среднего значения
			s.mu.Lock()
			for instrument, data := range s.instrumentsData {
				// Используем количество измерений для данного инструмента при вычислении среднего
				data.AvgPrice = avg / float64(countData[instrument])
			}
			s.mu.Unlock()
		}
	}
}

func (s *server) GetPrice(ctx context.Context, req *pb.PriceRequest) (*pb.PriceResponse, error) {
	instrument := req.GetInstrument()

	// Получаем текущую цену
	price, err := getPriceFromCoinGecko(instrument)
	if err != nil {
		return nil, err
	}

	// Преобразуем значение price в строку с шестью знаками после запятой
	priceStr := strconv.FormatFloat(price, 'f', 6, 64)

	// Преобразуем priceStr в float64
	priceFloat, _ := strconv.ParseFloat(priceStr, 64)

	// Захватываем мьютекс для безопасного доступа к данным инструмента
	s.mu.Lock()
	defer s.mu.Unlock()

	// Обновляем данные для инструмента
	data, ok := s.instrumentsData[instrument]
	if !ok {
		return nil, fmt.Errorf("инструмент не найден: %s", instrument)
	}
	data.CurrentPrice = priceFloat
	data.PriceHistory = append(data.PriceHistory, priceFloat)

	// Удаляем старые измерения, если их количество превышает 10
	if len(data.PriceHistory) > 10 {
		data.PriceHistory = data.PriceHistory[len(data.PriceHistory)-10:]
	}

	return &pb.PriceResponse{Price: priceFloat}, nil
}

func (s *server) GetAvg(ctx context.Context, req *pb.AvgRequest) (*pb.AvgResponse, error) {
	// Захватываем мьютекс для безопасного доступа к данным инструмента
	s.mu.RLock()
	defer s.mu.RUnlock()

	instrument := req.GetInstrument()
	data, ok := s.instrumentsData[instrument]
	if !ok {
		return nil, fmt.Errorf("для инструмента не найдены данные: %s", instrument)
	}

	// Преобразуем значение data.AvgPrice в строку с двумя знаками после запятой
	avgPriceStr := strconv.FormatFloat(data.AvgPrice, 'f', 2, 64)

	// Преобразуем avgPriceStr в float64
	avgPrice, _ := strconv.ParseFloat(avgPriceStr, 64)

	return &pb.AvgResponse{Avg: avgPrice}, nil

}
func (s *server) CollectAveragePrice(req *pb.CollectAveragePriceRequest, stream pb.CryptoPriceService_CollectAveragePriceServer) error {
	instrument := req.GetInstrument()
	intervalSeconds := req.GetIntervalSeconds()

	// Проверяем, что инструмент есть в списке инструментов
	_, ok := s.instrumentsData[instrument]
	if !ok {
		return fmt.Errorf("инструмент не найден: %s", instrument)
	}

	for {
		// Получаем текущую цену
		price, err := getPriceFromCoinGecko(instrument)
		if err != nil {
			log.Printf("ошибка при получении текущей цены: %v", err)
			continue
		}

		// Захватываем мьютекс для безопасного доступа к данным инструмента
		s.mu.Lock()
		data := s.instrumentsData[instrument]
		data.CurrentPrice = price
		data.PriceHistory = append(data.PriceHistory, price)
		s.mu.Unlock()

		// Если у нас уже больше 10 измерений, обрезаем исторические данные до 10
		if len(data.PriceHistory) > 10 {
			data.PriceHistory = data.PriceHistory[len(data.PriceHistory)-10:]
		}

		// Отправляем текущую цену в стрим
		if err := stream.Send(&pb.CollectAveragePriceResponse{Price: price}); err != nil {
			log.Printf("ошибка при отправке данных: %v", err)
		}

		// Ждем заданный интервал времени перед следующим измерением
		time.Sleep(time.Duration(intervalSeconds) * time.Second)
	}
}

func (s *server) SubscribeAlert(req *pb.SubscribeAlertRequest, stream pb.CryptoPriceService_SubscribeAlertServer) error {
	instrument := req.GetInstrument()
	threshold := req.GetThreshold()

	// Проверяем, что инструмент есть в списке инструментов
	_, ok := s.instrumentsData[instrument]
	if !ok {
		return fmt.Errorf("инструмент не найден: %s", instrument)
	}

	// Ожидаем пересечения порога текущей цены
	for {
		price, err := getPriceFromCoinGecko(instrument)
		if err != nil {
			return err
		}

		// Захватываем мьютекс для безопасного доступа к данным инструмента
		s.mu.Lock()
		data := s.instrumentsData[instrument]
		data.CurrentPrice = price
		data.PriceHistory = append(data.PriceHistory, price)
		s.mu.Unlock()

		// Если у нас уже больше 10 измерений, обрезаем исторические данные до 10
		if len(data.PriceHistory) > 10 {
			data.PriceHistory = data.PriceHistory[len(data.PriceHistory)-10:]
		}

		// Отправляем предыдущую и текущую цену в стрим, если произошло пересечение порога
		if len(data.PriceHistory) > 1 && ((data.PriceHistory[len(data.PriceHistory)-2] < threshold && price >= threshold) ||
			(data.PriceHistory[len(data.PriceHistory)-2] > threshold && price <= threshold)) {
			previousPrice := data.PriceHistory[len(data.PriceHistory)-2]
			if err := stream.Send(&pb.SubscribeAlertResponse{PreviousPrice: previousPrice, CurrentPrice: price}); err != nil {
				return err
			}
		}

		// Ждем некоторое время перед следующей проверкой
		time.Sleep(5 * time.Second)
	}
}

func main() {
	// Загрузка переменных окружения из файла .env
	err := godotenv.Load("E:/work/crypto_price_service/cmd/.env")
	if err != nil {
		log.Fatalf("Ошибка при загрузке файла .env: %v", err)
	}

	app := cli.NewApp()
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:    "port",
			Value:   "50051",
			Usage:   "Порт для привязки gRPC сервера",
			EnvVars: []string{"GRPC_PORT"},
		},
		&cli.StringFlag{
			Name:    "instruments",
			Value:   "bitcoin,etherium,bogdanoff",
			Usage:   "Список инструментов, разделенных запятыми",
			EnvVars: []string{"INSTRUMENTS"},
		},
	}

	app.Action = func(c *cli.Context) error {
		port := c.String("port")
		instrumentsStr := c.String("instruments")
		instruments := strings.Split(instrumentsStr, ",")

		// Инициализируем словарь для хранения данных о ценах каждого инструмента
		instrumentsData := make(map[string]*instrumentData)
		for _, instrument := range instruments {
			instrumentsData[instrument] = &instrumentData{}
		}

		s := &server{
			instrumentsData: instrumentsData,
		}

		// Запускаем горутину для вычисления среднего значения по всем активам
		go s.calculateAverage()

		lis, err := net.Listen("tcp", ":"+port)
		if err != nil {
			return err
		}

		srv := grpc.NewServer()
		pb.RegisterCryptoPriceServiceServer(srv, s)
		log.Printf("Запуск сервиса Crypto Price на порту %s...", port)
		if err := srv.Serve(lis); err != nil {
			return err
		}

		return nil
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
